(ns desenhos-animados.core
  (:use seesaw.core
        seesaw.mig
        desenhos-animados.tela-pesquisa
        [seesaw.event :only [events-for]])
  (:import java.util.Calendar
           javax.swing.JFrame)
  (:gen-class))

(native!)
(defrecord DesenhoAnimado [id, nome, preco, ano, faixa-etaria, minutos, quantidade])

(def form-fields
  [:#txt_nome, :#spn_preco, :#spn_ano, :#spn_faixa_etaria, :#spn_minutos, :#spn_quantidade])

(def year (long (.get (Calendar/getInstance) Calendar/YEAR)))
(def empty-desenho (->DesenhoAnimado 0, "", 0.0, year, 0, 0, 0))
(def desenhos (atom [empty-desenho]))
(def current-id (atom 0))

(declare main-panel)

(defn id-to-component [id]
  (select main-panel [id]))

(defn controles-formulario []
  (map id-to-component form-fields))

(defn build-desenho
  "Constrói uma instancia de DesenhoAnimado a partir do formulário preenchido"
  [num_id]
  (apply ->DesenhoAnimado
         (conj (map value (controles-formulario)) num_id)))

(defn clear-form "Limpa todos campos do formulario de dados" []
  (doseq [txt (select main-panel [:JTextField])]
    (value! txt ""))
  (doseq [txt (select main-panel [:JSpinner])]
    (value! txt 0))
  (value! (select main-panel [:#spn_ano]) year))

(defn check-changes
  "Verifica antes das operações de escrita se não vai perder o estado atual do formulário"
  ([current, index]
   (or (= (@desenhos (max 0 (min index ((comp dec count) @desenhos)))) current)
       (confirm main-panel
                "Você vai perder todas as mudanças tem certeza?"
                :type :warning)))
  ([current]
   (check-changes current @current-id)))

(defn recarregar-atual
  "Recarrega o formulário para representar o @current-id"
  []
  (doseq [pair (map vector (drop 1 (vals (@desenhos @current-id)))
                    (controles-formulario))]
    (value! (pair 1) (pair 0))))

(defn desenho-anterior
  "botao anterior"
  [_]
  (if (and (> @current-id 0) (check-changes (build-desenho @current-id)))
    (do (swap! current-id dec) (recarregar-atual))
    (alert main-panel "Já no primeiro desenho" :type :warning)))

(defn proximo-desenho
  "botao proximo"
  [_]
  (if (and (< @current-id ((comp dec count) @desenhos))
           (check-changes (build-desenho @current-id)))
    (do (swap! current-id inc) (recarregar-atual))
    (alert main-panel "Já no último desenho" :type :warning)))

(defn apagar-desenho "apaga o desenho selecionado." [_]
  (clear-form)
  (swap! desenhos assoc @current-id empty-desenho))

(defn novo-desenho [_]
  (let [current (build-desenho @current-id)]
    (when (and ((comp not =) current empty-desenho)
               (check-changes current (max (dec @current-id) 0)))
      (swap! current-id + (if (= empty-desenho current) 0 1))
      (clear-form))))

(defn salvar-desenho "Salva um desenho" [_]
  (swap! desenhos assoc @current-id (build-desenho @current-id)))

(defn pesquisar-desenho [_]
  (println (show! (mostra-pesquisa (to-root main-panel) @desenhos))))

(defn ir-para [_]
  (let [raiz (to-root main-panel)
        alvo (input raiz "Qual o ID você deseja selecionar?")]
    (try
      (let [numb (. Integer parseInt alvo)]
        (reset! current-id (max 0 (min ((comp dec count) @desenhos) numb)))
        (recarregar-atual))
      (catch NumberFormatException _ (alert raiz "Número inválido"
                                            :title "Erro"
                                            :icon :warning)))))

(def main-panel
  (mig-panel
    :constraints ["", "[][grow, fill]", "[grow]"]
    :items [
            [(label :text "<html><h1>Dados do Desenho</h1></html>"
                    :halign :center), "span 2, grow, wrap"]
            [(label "Nome do desenho")]
            [(text :columns 20 :id :txt_nome), "wrap"]
            [(label "Faixa etária")]
            [(spinner :id :spn_faixa_etaria
                      :model (spinner-model 0 :from 0 :to 18)) "wrap"]
            [(label "Minutos de duração")]
            [(spinner :id :spn_minutos
                      :model (spinner-model 0 :from 0 :to 9000)) "wrap"]
            [(label "Quantidade em estoque")]
            [(spinner :id :spn_quantidade
                      :model (spinner-model 0 :from 0)) "wrap"]
            [(label "Preço")]
            [(spinner :id :spn_preco
                      :model (spinner-model 0.0 :from 0.0)) "wrap"]
            [(label "Ano de criação")]
            [(spinner :id :spn_ano
                      :model (spinner-model year :from 1500 :to year)) "wrap"]
            [(button :text "Apagar" :listen [:action apagar-desenho])
             "growx, sg, span, split 3"]
            [(button :text "Novo" :listen [:action novo-desenho]) "growx, sg"]
            [(button :text "Salvar" :listen [:action salvar-desenho])
             "growx, sg, wrap"]
            [(button :text "Anterior" :listen [:action desenho-anterior])
             "growx, sg, span, split 3"]
            [(button :text "Ir para..." :listen [:action ir-para])
             "growx, sg"]
            [(button :text "Proximo" :listen [:action proximo-desenho])
             "growx, sg, wrap"]
            [(button :text "Metadados" :listen [:action pesquisar-desenho]),
             "span, growx"]]))

(defn- closing [_]
  (dispose! (to-root painel-pesquisa)))

(defn -main []
  (let [window-frame
        (frame :title "Editor de desenhos animados",
               :content main-panel,
               :on-close :dispose
               :listen [:window-closing closing])]
    (.pack window-frame)
    (.setMinimumSize window-frame (.getSize window-frame))
    (.setLocationRelativeTo window-frame nil)
    (invoke-later (-> window-frame show!))))
